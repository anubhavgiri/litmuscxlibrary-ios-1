//
//  NSURLProtocolCustom.m
//  LitmusCXLibrary
//
//  Created by admin on 15/12/17.
//

#import "NSURLProtocolCustom.h"

@implementation NSURLProtocolCustom


+ (BOOL)canInitWithRequest:(NSURLRequest*)theRequest
{
    if ([theRequest.URL.scheme caseInsensitiveCompare:@"myapp"] == NSOrderedSame) {
        return YES;
    }
    return NO;
}

+ (NSURLRequest*)canonicalRequestForRequest:(NSURLRequest*)theRequest
{
    return theRequest;
}

- (void)startLoading
{
    NSLog(@"%@", self.request.URL);
    
    NSBundle* podBundle = [NSBundle bundleForClass:[self class]];
    NSURL* bundleURL = [podBundle URLForResource:@"LitmusCXLibrary" withExtension:@"bundle"];
    NSBundle* frameWorkBundle = [NSBundle bundleWithURL:bundleURL];
    
    NSURLResponse *response = [[NSURLResponse alloc] initWithURL:[self.request URL]
                                                        MIMEType:@"image/gif"
                                           expectedContentLength:-1
                                                textEncodingName:nil];
    
    NSString *imagePath = [frameWorkBundle pathForResource:@"litmus_loader.gif" ofType:nil];
    NSData *data = [NSData dataWithContentsOfFile:imagePath];
    
    [[self client] URLProtocol:self didReceiveResponse:response cacheStoragePolicy:NSURLCacheStorageNotAllowed];
    [[self client] URLProtocol:self didLoadData:data];
    [[self client] URLProtocolDidFinishLoading:self];
}

- (void)stopLoading
{
    // Called when loading gets completed
    // NSLog(@"something went wrong!");
}

@end
