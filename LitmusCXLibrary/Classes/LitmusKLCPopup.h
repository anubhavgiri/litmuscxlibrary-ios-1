// KLCPopup.h
//
// Created by Jeff Mascia
// Copyright (c) 2013-2014 Kullect Inc. (http://kullect.com)
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

#import <UIKit/UIKit.h>
// LitmusKLCPopupShowType: Controls how the popup will be presented.
typedef NS_ENUM(NSInteger, LitmusKLCPopupShowType) {
	LitmusKLCPopupShowTypeNone = 0,
	LitmusKLCPopupShowTypeFadeIn,
  LitmusKLCPopupShowTypeGrowIn,
  LitmusKLCPopupShowTypeShrinkIn,
  LitmusKLCPopupShowTypeSlideInFromTop,
  LitmusKLCPopupShowTypeSlideInFromBottom,
  LitmusKLCPopupShowTypeSlideInFromLeft,
  LitmusKLCPopupShowTypeSlideInFromRight,
  LitmusKLCPopupShowTypeBounceIn,
  LitmusKLCPopupShowTypeBounceInFromTop,
  LitmusKLCPopupShowTypeBounceInFromBottom,
  LitmusKLCPopupShowTypeBounceInFromLeft,
  LitmusKLCPopupShowTypeBounceInFromRight,
};

// LitmusKLCPopupDismissType: Controls how the popup will be dismissed.
typedef NS_ENUM(NSInteger, LitmusKLCPopupDismissType) {
	LitmusKLCPopupDismissTypeNone = 0,
	LitmusKLCPopupDismissTypeFadeOut,
  LitmusKLCPopupDismissTypeGrowOut,
  LitmusKLCPopupDismissTypeShrinkOut,
  LitmusKLCPopupDismissTypeSlideOutToTop,
  LitmusKLCPopupDismissTypeSlideOutToBottom,
  LitmusKLCPopupDismissTypeSlideOutToLeft,
  LitmusKLCPopupDismissTypeSlideOutToRight,
  LitmusKLCPopupDismissTypeBounceOut,
  LitmusKLCPopupDismissTypeBounceOutToTop,
  LitmusKLCPopupDismissTypeBounceOutToBottom,
  LitmusKLCPopupDismissTypeBounceOutToLeft,
  LitmusKLCPopupDismissTypeBounceOutToRight,
};



// LitmusKLCPopupHorizontalLayout: Controls where the popup will come to rest horizontally.
typedef NS_ENUM(NSInteger, LitmusKLCPopupHorizontalLayout) {
  LitmusKLCPopupHorizontalLayoutCustom = 0,
  LitmusKLCPopupHorizontalLayoutLeft,
  LitmusKLCPopupHorizontalLayoutLeftOfCenter,
  LitmusKLCPopupHorizontalLayoutCenter,
  LitmusKLCPopupHorizontalLayoutRightOfCenter,
  LitmusKLCPopupHorizontalLayoutRight,
};

// LitmusKLCPopupVerticalLayout: Controls where the popup will come to rest vertically.
typedef NS_ENUM(NSInteger, LitmusKLCPopupVerticalLayout) {
  LitmusKLCPopupVerticalLayoutCustom = 0,
	LitmusKLCPopupVerticalLayoutTop,
  LitmusKLCPopupVerticalLayoutAboveCenter,
  LitmusKLCPopupVerticalLayoutCenter,
  LitmusKLCPopupVerticalLayoutBelowCenter,
  LitmusKLCPopupVerticalLayoutBottom,
};

// LitmusKLCPopupMaskType
typedef NS_ENUM(NSInteger, LitmusKLCPopupMaskType) {
	LitmusKLCPopupMaskTypeNone = 0, // Allow interaction with underlying views.
	LitmusKLCPopupMaskTypeClear, // Don't allow interaction with underlying views.
	LitmusKLCPopupMaskTypeDimmed, // Don't allow interaction with underlying views, dim background.
};

// LitmusKLCPopupLayout structure and maker functions
struct LitmusKLCPopupLayout {
  LitmusKLCPopupHorizontalLayout horizontal;
  LitmusKLCPopupVerticalLayout vertical;
};
typedef struct LitmusKLCPopupLayout LitmusKLCPopupLayout;

extern LitmusKLCPopupLayout LitmusKLCPopupLayoutMake(LitmusKLCPopupHorizontalLayout horizontal, LitmusKLCPopupVerticalLayout vertical);

extern const LitmusKLCPopupLayout LitmusKLCPopupLayoutCenter;



@interface LitmusKLCPopup : UIView

// This is the view that you want to appear in Popup.
// - Must provide contentView before or in willStartShowing.
// - Must set desired size of contentView before or in willStartShowing.
@property (nonatomic, strong) UIView* contentView;

// Animation transition for presenting contentView. default = shrink in
@property (nonatomic, assign) LitmusKLCPopupShowType showType;

// Animation transition for dismissing contentView. default = shrink out
@property (nonatomic, assign) LitmusKLCPopupDismissType dismissType;

// Mask prevents background touches from passing to underlying views. default = dimmed.
@property (nonatomic, assign) LitmusKLCPopupMaskType maskType;

// Overrides alpha value for dimmed background mask. default = 0.5
@property (nonatomic, assign) CGFloat dimmedMaskAlpha;

// If YES, then popup will get dismissed when background is touched. default = YES.
@property (nonatomic, assign) BOOL shouldDismissOnBackgroundTouch;

// If YES, then popup will get dismissed when content view is touched. default = NO.
@property (nonatomic, assign) BOOL shouldDismissOnContentTouch;

// Block gets called after show animation finishes. Be sure to use weak reference for popup within the block to avoid retain cycle.
@property (nonatomic, copy) void (^didFinishShowingCompletion)();

// Block gets called when dismiss animation starts. Be sure to use weak reference for popup within the block to avoid retain cycle.
@property (nonatomic, copy) void (^willStartDismissingCompletion)();

// Block gets called after dismiss animation finishes. Be sure to use weak reference for popup within the block to avoid retain cycle.
@property (nonatomic, copy) void (^didFinishDismissingCompletion)();

// Convenience method for creating popup with default values (mimics UIAlertView).
+ (LitmusKLCPopup*)popupWithContentView:(UIView*)contentView;

// Convenience method for creating popup with custom values.
+ (LitmusKLCPopup*)popupWithContentView:(UIView*)contentView
                         showType:(LitmusKLCPopupShowType)showType
                      dismissType:(LitmusKLCPopupDismissType)dismissType
                         maskType:(LitmusKLCPopupMaskType)maskType
         dismissOnBackgroundTouch:(BOOL)shouldDismissOnBackgroundTouch
            dismissOnContentTouch:(BOOL)shouldDismissOnContentTouch;

// Dismisses all the popups in the app. Use as a fail-safe for cleaning up.
+ (void)dismissAllPopups;

// Show popup with center layout. Animation determined by showType.
- (void)show;

// Show with specified layout.
- (void)showWithLayout:(LitmusKLCPopupLayout)layout;

// Show and then dismiss after duration. 0.0 or less will be considered infinity.
- (void)showWithDuration:(NSTimeInterval)duration;

// Show with layout and dismiss after duration.
- (void)showWithLayout:(LitmusKLCPopupLayout)layout duration:(NSTimeInterval)duration;

// Show centered at point in view's coordinate system. If view is nil use screen base coordinates.
- (void)showAtCenter:(CGPoint)center inView:(UIView*)view;

// Show centered at point in view's coordinate system, then dismiss after duration.
- (void)showAtCenter:(CGPoint)center inView:(UIView *)view withDuration:(NSTimeInterval)duration;

// Dismiss popup. Uses dismissType if animated is YES.
- (void)dismiss:(BOOL)animated;


#pragma mark Subclassing
@property (nonatomic, strong, readonly) UIView *backgroundView;
@property (nonatomic, strong, readonly) UIView *containerView;
@property (nonatomic, assign, readonly) BOOL isBeingShown;
@property (nonatomic, assign, readonly) BOOL isShowing;
@property (nonatomic, assign, readonly) BOOL isBeingDismissed;

- (void)willStartShowing;
- (void)didFinishShowing;
- (void)willStartDismissing;
- (void)didFinishDismissing;

@end


#pragma mark - UIView Category
@interface UIView(LitmusKLCPopup)
- (void)forEachPopupDoBlock:(void (^)(LitmusKLCPopup* popup))block;
- (void)dismissPresentingPopup;
@end

